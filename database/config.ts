import * as dotenv from 'dotenv';

dotenv.config();
const { DB_USERNAME, DB_PASSWORD, DB_NAME, DB_HOST, DB_PORT, DB_URI } = process.env;
export const DatabaseConfig = [
{
    type: 'mongodb',
    driver: 'mysql',
    uri: DB_URI,
    database: DB_NAME,
    port: parseInt(DB_PORT) || 27017,
    username: DB_USERNAME,
    password: DB_PASSWORD,
    host: DB_HOST,
    synchronize: false,
    migrationsRun: false,
    entities: ["dist/**/*.entity{.ts,.js}"],
    migrations: ["database/migrations/**/*{.ts,.js}", "database/seedings/**/*{.ts,.js}"],
    cli: { "migrationsDir": "database/migrations" },
}]

export default DatabaseConfig;