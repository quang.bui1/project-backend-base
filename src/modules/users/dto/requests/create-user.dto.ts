import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsString,
  MinLength,
} from 'class-validator';
import { Transform } from 'class-transformer';

export class CreateUserDto {
  @IsString({ message: 'First name must be string' })
  @IsOptional()
  @ApiProperty({ example: 'James', type: String })
  readonly firstName: string;

  @IsString({ message: 'Last name must be string' })
  @IsOptional()
  @ApiProperty({ example: 'Bond', type: String })
  readonly lastName: string;

  @IsEmail({}, { message: 'Email is invalid' })
  @IsNotEmpty()
  @Transform((email) => email.value.toLowerCase())
  @ApiProperty({ type: String, required: true })
  readonly email: string;

  @IsNotEmpty()
  @ApiProperty({ type: String, required: false })
  @IsString({ message: 'Gender must be string' })
  readonly gender: string;
}

export class UpdateUserDto extends CreateUserDto {
  @IsString({ message: 'First name must be string' })
  @IsOptional()
  @ApiProperty({ example: 'James', type: String })
  readonly firstName: string;

  @IsString({ message: 'Last name must be string' })
  @IsOptional()
  @ApiProperty({ example: 'Bond', type: String })
  readonly lastName: string;

  @IsEmail({}, { message: 'Email is invalid' })
  @IsOptional()
  @Transform((email) => email.value.toLowerCase())
  @ApiProperty({ type: String, required: true })
  readonly email: string;

  @IsOptional()
  @ApiProperty({ type: String, required: false })
  @IsString({ message: 'Gender must be string' })
  readonly gender: string;
}
