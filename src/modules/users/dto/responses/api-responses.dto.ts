import { ApiProperty } from '@nestjs/swagger';

import { ApiResponse } from '../../../common/classes/api.response';

import { User } from '../../../../db/models/user.entity';

export class UserResult extends ApiResponse<User> {
  @ApiProperty({ type: User })
  data: User;
  public static success(data: User) {
    const result = new UserResult();
    result.returnSuccess(data);

    return result;
  }
}

export class UserPaginationResult extends ApiResponse<User[]> {
  @ApiProperty({ type: User })
  data: User[];
  public static success(items: User[]) {
    const result = new UserPaginationResult();
    result.returnSuccess(items);

    return result;
  }
}
