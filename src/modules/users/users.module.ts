import {
  Global,
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { ConfigService } from '../common/services/config.service';
import { JwtGuard } from '../common/jwt.guard';
import { CommonModule } from '../common/common.module';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { MongooseModule } from '@nestjs/mongoose';
import { Student, StudentSchema, User, UserSchema } from '../../db/models';
import { UsersService } from './services/users.service';
import { UsersController } from './users.controller';
@Global()
@Module({
  imports: [
    EventEmitterModule.forRoot(),
    JwtModule.registerAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        secret: configService.secretAccessTokenKey,
        signOptions: { expiresIn: configService.accessTokenExpiredIn },
      }),
    }),
    CommonModule,
    MongooseModule.forFeature(
      [{ name: User.name, schema: UserSchema }],
      'default',
    ),
  ],
  providers: [UsersService, JwtGuard],
  controllers: [UsersController],
  exports: [UsersService],
})
export class UsersModule {}
