export enum UserStatus {
  ACTIVE = 'active',
  INACTIVE = 'inActive',
  BANNED = 'banned',
  DELETED = 'deleted',
  PENDING = 'pending',
}
export enum UserGender {
  FEMALE = 'female',
  male = 'male',
  other = 'other',
}
export enum UserRole {
  ADMIN = 'admin',
  SYSTEM = 'system',
  USER = 'user',
  SUPERVISOR = 'supervisor',
}
