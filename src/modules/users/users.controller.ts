import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  InternalServerErrorException,
  Param,
  Post,
  Put,
  Request,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiTags,
  ApiHeader,
} from '@nestjs/swagger';

import {
  UserPaginationResult,
  UserResult,
} from './dto/responses/api-responses.dto';

import { JwtGuard } from '../common/jwt.guard';
import { ErrorResponse } from '../common/classes/error.response';
import { CreateUserDto, UpdateUserDto } from './dto/requests/create-user.dto';
import { UsersService } from './services/users.service';

@Controller({ path: 'users' })
@ApiTags('Users')
@ApiBearerAuth()
@UseGuards(JwtGuard)
export class UsersController {
  constructor(private usersService: UsersService) {}
  @Get('')
  @ApiOkResponse({ type: UserPaginationResult })
  async index() {
    try {
      const users = await this.usersService.pagination(10, 1);

      return UserPaginationResult.success(users);
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  @Post('create')
  @ApiOkResponse({ type: UserResult })
  async create(@Body() data: CreateUserDto) {
    try {
      let user = await this.usersService.findByEmail(data.email);
      if (user) {
        const message = 'exists';
        return ErrorResponse.returnError(HttpStatus.BAD_REQUEST, message, []);
      }

      if (!user) {
        user = await this.usersService.create(data);
      }

      return UserResult.success(user);
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  @Put('update/:id')
  @ApiOkResponse({ type: UserResult })
  async update(@Param('id') id: string, @Body() data: UpdateUserDto) {
    try {
      let user = await this.usersService.findById(id);
      if (!user) {
        const message = 'notFound';
        return ErrorResponse.returnError(HttpStatus.NOT_FOUND, message, []);
      }

      await this.usersService.update(id, data);

      user = await this.usersService.findById(id);
      return UserResult.success(user);
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  @Delete('delete/:id')
  @ApiOkResponse({ type: UserResult })
  async delete(@Param('id') id: string) {
    try {
      let user = await this.usersService.findById(id);
      if (!user) {
        const message = 'notFound';
        return ErrorResponse.returnError(HttpStatus.NOT_FOUND, message, []);
      }

      await this.usersService.delete(id);

      user = await this.usersService.findById(id);
      return UserResult.success(user);
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  @Get(':id')
  @ApiOkResponse({ type: UserResult })
  async detail(@Param('id') id: string) {
    try {
      if (!id) {
        return ErrorResponse.returnError(HttpStatus.NOT_FOUND, 'notFound', []);
      }

      const user = await this.usersService.findById(id as string);
      if (!user) {
        return ErrorResponse.returnError(HttpStatus.NOT_FOUND, 'notFound', []);
      }

      return UserResult.success(user);
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }
}
