import {
  Injectable,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from '../../../db/models';
import { ConfigService } from '../../common/services/config.service';
import { Model } from 'mongoose';
import { UserStatus } from '../users.constant';
import { CreateUserDto, UpdateUserDto } from '../dto/requests/create-user.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name) private user: Model<UserDocument>,
    private readonly configService: ConfigService,
  ) {}

  async pagination(
    limit: number,
    page: number,
    query?: { gender: string; status: string },
  ) {
    try {
      const aggregate = await this.user.aggregate([
        {
          $project: {
            firstName: 1,
            lastName: 1,
            gender: 1,
            status: 1,
            email: 1,
          },
        },
        {
          $facet: {
            pageInfo: [{ $count: 'totalItem' }],
            items: [
              {
                $skip: page <= 1 ? 0 : (page - 1) * limit,
              },
              {
                $limit: limit,
              },
            ],
          },
        },
      ]);

      return aggregate;
    } catch (error) {
      throw error;
    }
  }

  public async findByEmail(email: string) {
    try {
      return this.user.findOne({ email }).exec();
    } catch (error) {
      throw error;
    }
  }

  public async findById(id: string) {
    try {
      return this.user.findOne({ _id: id }).exec();
    } catch (error) {
      throw error;
    }
  }
  public async create(data: CreateUserDto) {
    try {
      const user = await this.user.create(data);
      return user;
    } catch (error) {
      throw error;
    }
  }

  public async update(id: string, data: UpdateUserDto) {
    try {
      return this.user.updateOne({ _id: id }, { $set: { ...data } });
    } catch (error) {
      throw error;
    }
  }

  public async delete(id: string) {
    try {
      return this.user.updateOne(
        { _id: id },
        { $set: { status: UserStatus.DELETED } },
      );
    } catch (error) {
      throw error;
    }
  }
}
