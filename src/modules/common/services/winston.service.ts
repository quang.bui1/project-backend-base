import { Module } from '@nestjs/common';
import { WinstonModule as NestWinstonModule } from 'nest-winston';
import { ConfigService } from './config.service';
import * as winston from 'winston';
import 'winston-daily-rotate-file';

@Module({
  imports: [
    NestWinstonModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        return {
          level: configService.logLevel,
          format: winston.format.json(),
          transports: [
            new winston.transports.DailyRotateFile({
              filename: './logs/api-%DATE%.log',
              datePattern: 'YYYY-MM-DD',
              zippedArchive: true,
              maxSize: '20m',
              maxFiles: '14d',
            }),
          ],
        };
      },
    }),
  ],
  controllers: [],
  providers: [],
})
export class WinstonModule {}
