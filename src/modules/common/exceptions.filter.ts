import {
  Catch,
  ArgumentsHost,
  HttpException,
  Inject,
  BadRequestException,
  HttpStatus,
  InternalServerErrorException,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { Logger } from 'winston';
import { ApiResponse } from './classes/api.response';
import { uniqueId } from 'lodash';
import { ConfigService } from './services/config.service';
import { BaseExceptionFilter } from '@nestjs/core';

const handleInternalErrorException = async (
  exception: InternalServerErrorException,
  request: Request,
  logger: Logger,
) => {
  const logId = Date.now() + uniqueId();
  const message = `System error with id = ${logId}: ${exception.message}`;
  // write detail log to trace bug
  logger.error(message, {
    requestUrl: request.url,
    request: request.body,
    exception,
  });
  console.log({ exception });

  // return only logId
  return {
    code: HttpStatus.INTERNAL_SERVER_ERROR,
    message: 'Internal server error',
    errors: [],
  };
};
const handleBadRequestException = async (
  exception: BadRequestException,
  request: Request,
) => {
  const apiResult = new ApiResponse<unknown>();
  apiResult.returnError(HttpStatus.BAD_REQUEST, exception.message, []);
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const response = exception.getResponse() as any;
  const errors = response.message;

  if (Array.isArray(errors) && errors.length > 0) {
    apiResult.errors = errors;
  }
  return apiResult;
};

@Catch(HttpException)
export class HttpExceptionFilter extends BaseExceptionFilter {
  constructor(
    @Inject('winston') private readonly logger: Logger,
    private readonly configService: ConfigService,
  ) {
    super();
  }
  async catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const request = ctx.getRequest<Request>();
    const response = ctx.getResponse<Response>();

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const apiResponse = exception.getResponse() as any;
    const status = exception.getStatus();
    let res = {
      code: exception.getStatus(),
      message: 'Error',
      errors: apiResponse?.errors || [],
    };
    this.logger.error(apiResponse.message, {
      requestUrl: request.url,
      request: request.body,
      exception,
    });
    if (exception instanceof InternalServerErrorException) {
      res = await handleInternalErrorException(exception, request, this.logger);
    } else if (exception instanceof BadRequestException) {
      res = await handleBadRequestException(exception, request);
    }
    return response.status(status).json(res);
  }
}
