import { ApiProperty } from '@nestjs/swagger';
import { HttpStatus, Injectable } from '@nestjs/common';

const DEFAULT_SUCCESS_MESSAGE = 'success';

@Injectable()
export class ApiResponse<T> {
  @ApiProperty()
  public code: number;

  @ApiProperty()
  public message: string;

  @ApiProperty()
  public data: T;

  @ApiProperty()
  public errors: T;

  public async returnSuccess(data?: T, message?: string) {
    this.code = HttpStatus.OK;
    this.message = message || DEFAULT_SUCCESS_MESSAGE;
    this.data = data;

    return this;
  }

  public async returnError(code: number, message: string, errors: T) {
    this.message = message;
    this.code = code;
    this.errors = errors;
    return this;
  }
}
