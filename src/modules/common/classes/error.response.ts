import { ApiResponse } from './api.response';

interface IErrorItem {
  field: string;
  value: string | number | boolean;
  errors: string[];
}
export class ErrorResponse extends ApiResponse<unknown> {
  public static returnError(
    code: number,
    message: string,
    errors?: Array<IErrorItem>,
  ) {
    const result = new ApiResponse();
    result.returnError(code, message, errors || []);
    return result;
  }
}
