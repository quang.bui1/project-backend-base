import {
  CallHandler,
  ExecutionContext,
  HttpStatus,
  Injectable,
  NestInterceptor,
  RequestMethod,
} from '@nestjs/common';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';
import { instanceToPlain } from 'class-transformer';

@Injectable()
export class TransformInterceptor implements NestInterceptor {
  intercept(
    context: ExecutionContext,
    next: CallHandler<unknown>,
  ): Observable<unknown> {
    const request = context.switchToHttp().getRequest<Request>();
    const requestMethod = request.method.toUpperCase();
    if (RequestMethod.POST === RequestMethod[requestMethod]) {
      context.switchToHttp().getResponse().status(HttpStatus.OK);
    }
    return next.handle().pipe(
      map((data) => {
        const json = JSON.parse(JSON.stringify(data));
        return instanceToPlain(json);
      }),
    );
  }
}
