import { ApiProperty } from '@nestjs/swagger';

import { ApiResponse } from '../../../common/classes/api.response';

import { Student } from '../../../../db/models/student.entity';

export class StudentResult extends ApiResponse<Student> {
  @ApiProperty({ type: Student })
  data: Student;
  public static success(data: Student) {
    const result = new StudentResult();
    result.returnSuccess(data);

    return result;
  }
}

export class StudentPaginationResult extends ApiResponse<Student[]> {
  @ApiProperty({ type: Student })
  data: Student[];
  public static success(items: Student[]) {
    const result = new StudentPaginationResult();
    result.returnSuccess(items);

    return result;
  }
}
