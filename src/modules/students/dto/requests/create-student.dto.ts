import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsMongoId,
  IsNotEmpty,
  IsOptional,
  IsPositive,
  IsString,
  Max,
  Min,
  MinLength,
} from 'class-validator';
import { Transform } from 'class-transformer';

export class CreateStudentDto {
  @IsString({ message: 'Full name must be string' })
  @ApiProperty({ example: 'Henry Bui', type: String })
  readonly fullName: string;

  @IsPositive({ message: 'Age is greater than 5, less than 60' })
  @ApiProperty({ type: Number, required: false })
  @Max(60)
  @Min(5)
  readonly age: Number;

  @IsNotEmpty()
  @ApiProperty({ type: String, required: false })
  @IsString({ message: 'Gender must be string' })
  readonly gender: String;

  @IsPositive({ message: 'mathScore is greater than 0, less than 10' })
  @ApiProperty({ type: Number, required: false })
  @Max(10)
  @Min(0)
  readonly mathScore: Number;

  @IsPositive({ message: 'englishScore is greater than 0, less than 10' })
  @ApiProperty({ type: Number, required: false })
  @Max(10)
  @Min(0)
  readonly englishScore: Number;

  @ApiProperty({ example: '', type: String, required: false })
  readonly class: string;
}

export class UpdateStudentDto extends CreateStudentDto {
  @IsString({ message: 'Full name must be string' })
  @ApiProperty({ example: 'Henry Bui', type: String })
  readonly fullName: string;

  @IsPositive({ message: 'Age is greater than 5, less than 60' })
  @ApiProperty({ type: Number, required: false })
  @Max(60)
  @Min(5)
  readonly age: Number;

  @IsNotEmpty()
  @ApiProperty({ type: String, required: false })
  @IsString({ message: 'Gender must be string' })
  readonly gender: string;

  @IsPositive({ message: 'mathScore is greater than 0, less than 10' })
  @ApiProperty({ type: Number, required: false })
  @Max(10)
  @Min(0)
  readonly mathScore: Number;

  @IsPositive({ message: 'englishScore is greater than 0, less than 10' })
  @ApiProperty({ type: Number, required: false })
  @Max(10)
  @Min(0)
  readonly englishScore: Number;

  @ApiProperty({ example: '', type: String, required: false })
  readonly class: string;
}
