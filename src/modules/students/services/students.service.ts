import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Student, StudentDocument } from 'src/db/models';
import { Model } from 'mongoose';
import { ConfigService } from 'src/modules/common/services/config.service';
import {
  CreateStudentDto,
  UpdateStudentDto,
} from '../dto/requests/create-student.dto';

@Injectable()
export class StudentsService {
  constructor(
    @InjectModel(Student.name) private student: Model<StudentDocument>,
    private readonly configService: ConfigService,
  ) {}

  async pagination(
    limit: number,
    page: number,
    query?: { gender: string; status: string },
  ) {
    try {
      const aggregate = await this.student.aggregate([
        { $match: { deleted: false } },
        {
          $lookup: {
            from: 'classes',
            localField: 'class',
            foreignField: '_id',
            as: 'class',
          },
        },
        {
          $unwind: '$class',
        },
        {
          $project: {
            fullName: 1,
            age: 1,
            gender: 1,
            mathScore: 1,
            englishScore: 1,
            class: 1,
          },
        },
        {
          $facet: {
            pageInfo: [{ $count: 'totalItem' }],
            items: [
              {
                $skip: page <= 1 ? 0 : (page - 1) * limit,
              },
              {
                $limit: limit,
              },
            ],
          },
        },
      ]);

      return aggregate;
    } catch (error) {
      throw error;
    }
  }

  public async findById(id: string) {
    try {
      return this.student.findOne({ _id: id }).exec();
    } catch (error) {
      throw error;
    }
  }
  public async create(data: CreateStudentDto) {
    try {
      const student = await this.student.create(data);
      return student;
    } catch (error) {
      throw error;
    }
  }

  public async update(id: string, data: UpdateStudentDto) {
    try {
      return this.student.updateOne({ _id: id }, { $set: { ...data } });
    } catch (error) {
      throw error;
    }
  }

  public async delete(id: string) {
    try {
      return this.student.updateOne({ _id: id }, { $set: { deleted: true } });
    } catch (error) {
      throw error;
    }
  }
}
