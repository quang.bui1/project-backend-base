import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  InternalServerErrorException,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { ErrorResponse } from '../common/classes/error.response';
import { TaskService } from '../task/task.service';
import { CreateUserDto } from '../users/dto/requests/create-user.dto';
import {
  CreateStudentDto,
  UpdateStudentDto,
} from './dto/requests/create-student.dto';
import {
  StudentPaginationResult,
  StudentResult,
} from './dto/responses/api-responses.dto';
import { StudentsService } from './services/students.service';

@ApiTags('Students')
@Controller('students')
export class StudentsController {
  constructor(
    private studentsService: StudentsService,
    private taskService: TaskService,
  ) {}

  @Get('')
  @ApiOkResponse({ type: StudentPaginationResult })
  async index() {
    try {
      const students = await this.studentsService.pagination(10, 1);
      this.taskService.handleCron();
      return StudentPaginationResult.success(students);
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  @Post('create')
  @ApiOkResponse({ type: StudentResult })
  async create(@Body() data: CreateStudentDto) {
    try {
      let student = await this.studentsService.create(data);
      return StudentResult.success(student);
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  @Patch('update/:id')
  @ApiOkResponse({ type: StudentResult })
  async update(@Param('id') id: string, @Body() data: UpdateStudentDto) {
    try {
      await this.studentsService.update(id, data);

      let student = await this.studentsService.findById(id);
      return StudentResult.success(student);
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  @Delete('delete/:id')
  @ApiOkResponse({ type: StudentResult })
  async delete(@Param('id') id: string) {
    try {
      let student = await this.studentsService.findById(id);
      if (!student) {
        const message = 'notFound';
        return ErrorResponse.returnError(HttpStatus.NOT_FOUND, message, []);
      }

      await this.studentsService.delete(id);

      student = await this.studentsService.findById(id);
      return StudentResult.success(student);
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  @Get(':id')
  @ApiOkResponse({ type: StudentResult })
  async detail(@Param('id') id: string) {
    try {
      if (!id) {
        return ErrorResponse.returnError(HttpStatus.NOT_FOUND, 'notFound', []);
      }

      const user = await this.studentsService.findById(id as string);
      if (!user) {
        return ErrorResponse.returnError(HttpStatus.NOT_FOUND, 'notFound', []);
      }

      return StudentResult.success(user);
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }
}
