import { Module } from '@nestjs/common';
import { StudentsController } from './students.controller';
import { StudentsService } from './services/students.service';
import { CommonModule } from '../common/common.module';
import { MongooseModule } from '@nestjs/mongoose';
import { Student, StudentSchema } from 'src/db/models';
import { TaskService } from '../task/task.service';

@Module({
  imports: [
    CommonModule,
    MongooseModule.forFeature(
      [{ name: Student.name, schema: StudentSchema }],
      'default',
    ),
  ],
  controllers: [StudentsController],
  providers: [StudentsService, TaskService],
})
export class StudentsModule {}
