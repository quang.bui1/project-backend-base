export const AUTHORIZATION_TYPE = 'refresh_token';

export enum UserTokenType {
  REFRESH_TOKEN = 'refresh_token',
}

export enum AuthEvent {
  LOGINED = 'auth.logined',
  SING_UP = 'auth.signUp',
}

export enum UserStatus {
  ACTIVE = 'active',
  INACTIVE = 'inActive',
  BANNED = 'banned',
  DELETED = 'deleted',
  PENDING = 'pending',
}
export enum UserGender {
  FEMALE = 'female',
  male = 'male',
  other = 'other',
}
export enum UserRole {
  ADMIN = 'admin',
  SYSTEM = 'system',
  USER = 'user',
}
