import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import {
  User,
  UserDocument,
  UserToken,
  UserTokenDocument,
} from '../../../db/models';
import { ConfigService } from '../../common/services/config.service';
import { EventEmitter2 } from 'eventemitter2';
import { AuthLoginedEvent } from '../events/auth-logined.event';
import { Model } from 'mongoose';
import { UserRole, UserStatus } from '../auth.constant';

@Injectable()
export class AuthService {
  constructor(
    @InjectModel(User.name) private user: Model<UserDocument>,
    @InjectModel(UserToken.name) private token: Model<UserTokenDocument>,
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
    private event: EventEmitter2,
  ) {}

  private generatePassword(password: string) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(10));
  }

  private generateHashToken(userId: number): string {
    const random = Math.floor(Math.random() * (10000 - 1000) + 1000);
    return `${userId}-${Date.now()}-${random}`;
  }

  async validatePassword(
    password: string,
    passwordHashed: string,
  ): Promise<boolean> {
    return bcrypt.compare(password, passwordHashed);
  }

  /**
   *
   * @param user
   * @return accessToken & accessTokenExpiredIn
   */
  private generateAccessToken(user: UserDocument) {
    const { accessTokenExpiredIn, secretAccessTokenKey } = this.configService;
    const accessTokenOptions = {
      secret: secretAccessTokenKey,
      expiresIn: accessTokenExpiredIn,
    };
    const payloadAccessToken = {
      id: user.id,
      email: user.email,
      role: user.role,
      expiresIn: accessTokenExpiredIn,
    };
    const accessToken = this.jwtService.sign(
      payloadAccessToken,
      accessTokenOptions,
    );
    return {
      token: accessToken,
      expiresIn: accessTokenExpiredIn,
    };
  }
  /**
   *
   * @param user
   * @param hashToken
   * @return refreshToken && refreshTokenExpiredIn
   */
  private generateRefreshToken(user: UserDocument, hashToken: string) {
    const { refreshTokenExpiredIn, secretRefreshTokenKey } = this.configService;
    const accessTokenOptions = {
      secret: secretRefreshTokenKey,
      expiresIn: refreshTokenExpiredIn,
    };

    const payloadAccessToken = {
      id: user.id,
      email: user.email,
      role: user.role,
      expiresIn: refreshTokenExpiredIn,
      hashToken,
    };
    const refreshToken = this.jwtService.sign(
      payloadAccessToken,
      accessTokenOptions,
    );
    return {
      token: refreshToken,
      expiresIn: refreshTokenExpiredIn,
    };
  }

  /**
   *
   * @param user User
   * @returns {user, accessToken, refreshToken}
   */

  public async login(user: UserDocument) {
    try {
      const accessToken = this.generateAccessToken(user);
      const hashToken = this.generateHashToken(user.id);
      const refreshToken = this.generateRefreshToken(user, hashToken);
      await this.token.create({ user, token: refreshToken.token, hashToken });

      // emit an event login success
      const authLoginedEvent = new AuthLoginedEvent();
      authLoginedEvent.id = user.id;
      this.event.emit('auth.logined', authLoginedEvent);

      return {
        user: user,
        accessToken,
        refreshToken,
      };
    } catch (error) {
      throw error;
    }
  }

  public async findById(id: string) {
    const user = this.user.findById(id);
    return user;
  }

  public async findUserByEmail(email: string): Promise<UserDocument> {
    try {
      return this.user.findOne({ email }).exec();
    } catch (error) {
      throw error;
    }
  }

  public async logout(user: User): Promise<boolean> {
    try {
      // delete old refresh token
      await this.token.remove({ user });
      return true;
    } catch (error) {
      throw error;
    }
  }

  public async refreshToken(user: UserDocument) {
    try {
      await this.token.deleteMany({ user: user.id });
      const accessToken = this.generateAccessToken(user);
      const hashToken = this.generateHashToken(user.id);
      const refreshToken = this.generateRefreshToken(user, hashToken);

      await this.token.create({
        user: user.id,
        token: refreshToken.token,
        hashToken,
      });
      const authLoginedEvent = new AuthLoginedEvent();
      authLoginedEvent.id = user.id;
      this.event.emit('auth.logined', authLoginedEvent);

      return {
        user,
        accessToken,
        refreshToken,
      };
    } catch (error) {
      throw error;
    }
  }

  async createUser(data: { email: string; password: string }) {
    try {
      data = Object.assign(data, {
        status: UserStatus.ACTIVE,
        role: UserRole.ADMIN,
        password: this.generatePassword(data.password),
      });

      const user = await this.user.create(data);
      return user;
    } catch (error) {
      throw error;
    }
  }
}
