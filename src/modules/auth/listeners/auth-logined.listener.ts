import { Injectable } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserDocument } from '../../../db/models';
import { AuthLoginedEvent } from '../events/auth-logined.event';

@Injectable()
export class AuthLoginedListener {
  constructor(@InjectModel(User.name) private user: Model<UserDocument>) {}
  @OnEvent('auth.logined')
  async handleAuthLoginedEvent(event: AuthLoginedEvent) {
    await this.user.updateOne(
      { id: event.id },
      { $set: { lastLoginAt: new Date() } },
    );
  }
}
