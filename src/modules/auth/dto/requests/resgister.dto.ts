import { Optional } from '@nestjs/common';
import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString, MinLength } from 'class-validator';

export class RegisterDto {
  @ApiProperty({ type: String, description: 'First name', default: 'James' })
  @Optional()
  @IsString()
  readonly firstName: string;

  @ApiProperty({ type: String, description: 'Last name', default: 'Bond' })
  @Optional()
  @IsString()
  readonly lastName: string;

  @ApiProperty({
    type: String,
    description: 'Email',
    default: 'admin@admin.com',
  })
  @IsEmail()
  @IsNotEmpty()
  readonly email: string;

  @ApiProperty({ type: String, description: 'Password', default: 'admin@1234' })
  @IsNotEmpty()
  @MinLength(6)
  readonly password: string;
}
