import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, MinLength } from 'class-validator';

export class LoginDto {
  @ApiProperty({
    type: String,
    description: 'Email',
    default: 'admin@admin.com',
  })
  @IsEmail()
  @IsNotEmpty()
  readonly email: string;

  @ApiProperty({ type: String, description: 'Password', default: 'admin@1234' })
  @IsNotEmpty()
  @MinLength(6)
  readonly password: string;
}
