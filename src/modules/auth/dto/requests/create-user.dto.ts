import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsNotEmpty,
  IsOptional,
  IsString,
  MinLength,
} from 'class-validator';
import { Transform } from 'class-transformer';

export class CreateUserDto {
  @IsString({ message: 'Name must be string' })
  @IsOptional()
  @ApiProperty({ example: 'Alex', type: String })
  readonly name: string;

  @IsEmail({}, { message: 'INVALID_EMAIL' })
  @IsNotEmpty()
  @Transform((email) => email.value.toLowerCase())
  @ApiProperty({ type: String, required: true })
  readonly email: string;

  @IsNotEmpty()
  @ApiProperty({ type: String, required: true })
  @MinLength(6, { message: 'Minimun 6 characters' })
  readonly password: string;
}
