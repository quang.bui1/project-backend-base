import { ApiProperty } from '@nestjs/swagger';

import { ApiResponse } from '../../../common/classes/api.response';

import { User } from '../../../../db/models/user.entity';

export interface ITokenInfo {
  token: string;
  expiresIn: number;
}
export class UserWithToken {
  @ApiProperty()
  profile: User;
  @ApiProperty()
  accessToken: ITokenInfo;
  @ApiProperty()
  refreshToken: ITokenInfo;
}

export class UserLoginResult extends ApiResponse<UserWithToken> {
  @ApiProperty({ type: UserWithToken })
  data: UserWithToken;

  public static returnSuccess(data: UserWithToken) {
    const result = new UserLoginResult();
    result.returnSuccess(data);

    return result;
  }
}

export class UserProfileResult extends ApiResponse<User> {
  @ApiProperty({ type: User })
  data: User;
  public static success(data: User) {
    const result = new UserProfileResult();
    result.returnSuccess(data);

    return result;
  }
}

export class UserLogoutResult extends ApiResponse<boolean> {
  data: boolean;
  public static success(data: boolean) {
    const result = new UserLogoutResult();
    result.returnSuccess(data);

    return result;
  }
}

export class RefreshTokenResult extends ApiResponse<UserWithToken> {
  data: UserWithToken;
  public static success(data: UserWithToken) {
    const result = new RefreshTokenResult();
    result.returnSuccess(data);

    return result;
  }
}
