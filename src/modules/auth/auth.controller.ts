import {
  Body,
  Controller,
  Get,
  HttpStatus,
  InternalServerErrorException,
  Post,
  Req,
  Request,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiOkResponse,
  ApiTags,
  ApiHeader,
} from '@nestjs/swagger';

import {
  UserLoginResult,
  UserLogoutResult,
  UserProfileResult,
  RefreshTokenResult,
} from './dto/responses/api-responses.dto';
import { LoginDto } from './dto/requests/login.dto';

import { AuthService } from './services/auth.service';
import { JwtGuard } from '../common/jwt.guard';
import { ErrorResponse } from '../common/classes/error.response';
import { UserStatus } from '../users/users.constant';
import { RegisterDto } from './dto/requests/resgister.dto';

@Controller({
  path: 'auth',
})
@ApiTags('Auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('login')
  @ApiOkResponse({ type: UserLoginResult })
  async login(@Body() data: LoginDto) {
    try {
      const user = await this.authService.findUserByEmail(data.email);
      // check if user exists?
      if (!user) {
        const message = 'notFound';
        return ErrorResponse.returnError(HttpStatus.NOT_FOUND, message, []);
      }

      // check if user is active?
      if (user.status !== UserStatus.ACTIVE) {
        const message = 'inActive';
        return ErrorResponse.returnError(HttpStatus.BAD_REQUEST, message, []);
      }
      // check password is correct?
      const isCorrectPassword = await this.authService.validatePassword(
        data.password,
        user.password,
      );

      if (!isCorrectPassword) {
        const message = 'invalidPwd';
        return ErrorResponse.returnError(HttpStatus.BAD_REQUEST, message, []);
      }
      // every thing ok, return success data
      const {
        user: profile,
        accessToken,
        refreshToken,
      } = await this.authService.login(user);
      return UserLoginResult.returnSuccess({
        profile,
        accessToken,
        refreshToken,
      });
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  @Post('register')
  @ApiOkResponse({ type: UserLoginResult })
  async register(@Body() data: RegisterDto) {
    try {
      let user = await this.authService.findUserByEmail(data.email);
      if (user) {
        const message = 'exists';
        return ErrorResponse.returnError(HttpStatus.BAD_REQUEST, message, []);
      }

      if (!user) {
        user = await this.authService.createUser(data);
      }

      // every thing ok, return success data
      const {
        user: profile,
        accessToken,
        refreshToken,
      } = await this.authService.login(user);
      return UserLoginResult.returnSuccess({
        profile,
        accessToken,
        refreshToken,
      });
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  @ApiBearerAuth()
  @UseGuards(JwtGuard)
  @Get('profile')
  @ApiOkResponse({ type: UserProfileResult })
  async profile(@Request() req) {
    try {
      if (!req.user) {
        return ErrorResponse.returnError(HttpStatus.NOT_FOUND, 'notFound', []);
      }

      const user = await this.authService.findById(req.user.id as string);
      if (!user) {
        return ErrorResponse.returnError(HttpStatus.NOT_FOUND, 'notFound', []);
      }

      return UserProfileResult.success(user);
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  @Post('refresh-token')
  @UseGuards(JwtGuard)
  @ApiBearerAuth()
  @ApiOkResponse({ type: RefreshTokenResult })
  async refreshToken(@Req() req) {
    try {
      const user = await this.authService.findById(req.user.id);
      const {
        user: profile,
        accessToken,
        refreshToken,
      } = await this.authService.refreshToken(user);

      return RefreshTokenResult.success({ profile, accessToken, refreshToken });
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  @Post('logout')
  @UseGuards(JwtGuard)
  @ApiBearerAuth()
  @UsePipes(new ValidationPipe({ transform: true }))
  @ApiOkResponse({ type: UserLogoutResult })
  async logout(@Request() req) {
    try {
      const result = await this.authService.logout(req.user);
      return UserLogoutResult.success(result);
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }
}
