import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  InternalServerErrorException,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { ErrorResponse } from '../common/classes/error.response';
import {
  CreateClassDto,
  UpdateClassDto,
} from './dto/requests/create-class.dto';
import {
  ClassPaginationResult,
  ClassResult,
} from './dto/responses/api-responses.dto';
import { ClassService } from './services/classes.service';

@ApiTags('Classes')
@Controller('class')
export class ClassController {
  constructor(private classService: ClassService) {}
  @Get('')
  @ApiOkResponse({ type: ClassPaginationResult })
  async index() {
    try {
      const students = await this.classService.pagination(10, 1);

      return ClassPaginationResult.success(students);
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  @Post('create')
  @ApiOkResponse({ type: ClassResult })
  async create(@Body() data: CreateClassDto) {
    try {
      let classRef = await this.classService.create(data);
      return ClassResult.success(classRef);
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  @Patch('update/:id')
  @ApiOkResponse({ type: ClassResult })
  async update(@Param('id') id: string, @Body() data: UpdateClassDto) {
    try {
      await this.classService.update(id, data);

      let classRef = await this.classService.findById(id);
      return ClassResult.success(classRef);
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  @Delete('delete/:id')
  @ApiOkResponse({ type: ClassResult })
  async delete(@Param('id') id: string) {
    try {
      let classRef = await this.classService.findById(id);
      if (!classRef) {
        const message = 'notFound';
        return ErrorResponse.returnError(HttpStatus.NOT_FOUND, message, []);
      }

      await this.classService.delete(id);

      classRef = await this.classService.findById(id);
      return ClassResult.success(classRef);
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  @Get(':id')
  @ApiOkResponse({ type: ClassResult })
  async detail(@Param('id') id: string) {
    try {
      if (!id) {
        return ErrorResponse.returnError(HttpStatus.NOT_FOUND, 'notFound', []);
      }

      const classRef = await this.classService.findById(id as string);
      if (!classRef) {
        return ErrorResponse.returnError(HttpStatus.NOT_FOUND, 'notFound', []);
      }

      return ClassResult.success(classRef);
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }
}
