import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Class, ClassDocument } from 'src/db/models';
import { Model } from 'mongoose';
import { ConfigService } from 'src/modules/common/services/config.service';
import {
  CreateClassDto,
  UpdateClassDto,
} from '../dto/requests/create-class.dto';

@Injectable()
export class ClassService {
  constructor(
    @InjectModel(Class.name) private classRef: Model<ClassDocument>,
    private readonly configService: ConfigService,
  ) {}

  async pagination(
    limit: number,
    page: number,
    query?: { gender: string; status: string },
  ) {
    try {
      const aggregate = await this.classRef.aggregate([
        { $match: { deleted: false } },
        {
          $lookup: {
            from: 'students',
            localField: '_id',
            foreignField: 'class',
            as: 'students',
          },
        },
        {
          $project: {
            className: 1,
            schoolName: 1,
            teacherName: 1,
            totalStudent: { $size: '$students' },
            highMathScore: { $max: '$students.mathScore' },
            highEnglishScore: { $max: '$students.englishScore' },
          },
        },
        {
          $facet: {
            pageInfo: [{ $count: 'totalItem' }],
            items: [
              {
                $skip: page <= 1 ? 0 : (page - 1) * limit,
              },
              {
                $limit: limit,
              },
            ],
          },
        },
      ]);

      return aggregate;
    } catch (error) {
      throw error;
    }
  }

  public async findById(id: string) {
    try {
      return this.classRef.findOne({ _id: id }).exec();
    } catch (error) {
      throw error;
    }
  }
  public async create(data: CreateClassDto) {
    try {
      const classRef = await this.classRef.create(data);
      return classRef;
    } catch (error) {
      throw error;
    }
  }

  public async update(id: string, data: UpdateClassDto) {
    try {
      return this.classRef.updateOne({ _id: id }, { $set: { ...data } });
    } catch (error) {
      throw error;
    }
  }

  public async delete(id: string) {
    try {
      return this.classRef.updateOne({ _id: id }, { $set: { deleted: true } });
    } catch (error) {
      throw error;
    }
  }
}
