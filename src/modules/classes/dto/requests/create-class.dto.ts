import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsMongoId,
  IsNotEmpty,
  IsOptional,
  IsPositive,
  IsString,
  Max,
  Min,
  MinLength,
} from 'class-validator';
import { Transform } from 'class-transformer';

export class CreateClassDto {
  @IsString({ message: 'Class name must be string' })
  @ApiProperty({ example: 'Class Class', type: String })
  readonly className: string;

  @IsString({ message: 'School name must be string' })
  @ApiProperty({ example: 'School School', type: String })
  readonly schoolName: string;

  @IsString({ message: 'Teacher name must be string' })
  @ApiProperty({ example: 'Teacher Teacher', type: String })
  readonly teacherName: string;
}

export class UpdateClassDto extends CreateClassDto {
  @IsString({ message: 'Class name must be string' })
  @ApiProperty({ example: 'Class Class', type: String })
  readonly className: string;

  @IsString({ message: 'School name must be string' })
  @ApiProperty({ example: 'School School', type: String })
  readonly schoolName: string;

  @IsString({ message: 'Teacher name must be string' })
  @ApiProperty({ example: 'Teacher Teacher', type: String })
  readonly teacherName: string;
}
