import { ApiProperty } from '@nestjs/swagger';

import { ApiResponse } from '../../../common/classes/api.response';

import { Class } from '../../../../db/models/class.entity';

export class ClassResult extends ApiResponse<Class> {
  @ApiProperty({ type: Class })
  data: Class;
  public static success(data: Class) {
    const result = new ClassResult();
    result.returnSuccess(data);
    return result;
  }
}

export class ClassPaginationResult extends ApiResponse<Class[]> {
  @ApiProperty({ type: Class })
  data: Class[];
  public static success(items: Class[]) {
    const result = new ClassPaginationResult();
    result.returnSuccess(items);

    return result;
  }
}
