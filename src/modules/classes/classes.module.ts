import { Module } from '@nestjs/common';
import { ClassController } from './classes.controller';
import { ClassService } from './services/classes.service';
import { CommonModule } from '../common/common.module';
import { MongooseModule } from '@nestjs/mongoose';
import { Class, ClassSchema, Student, StudentSchema } from 'src/db/models';

@Module({
  imports: [
    CommonModule,
    MongooseModule.forFeature(
      [{ name: Class.name, schema: ClassSchema }],
      'default',
    ),
  ],
  controllers: [ClassController],
  providers: [ClassService],
})
export class ClassModule {}
