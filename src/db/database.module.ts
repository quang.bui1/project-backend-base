import { Global, Module } from '@nestjs/common';
import { CommonModule } from '../modules/common/common.module';
import { ConfigService } from '../modules/common/services/config.service';
import { MongooseModule, MongooseModuleOptions } from '@nestjs/mongoose';

@Global()
@Module({
  imports: [
    CommonModule,
    MongooseModule.forRootAsync({
      imports: [CommonModule],
      connectionName: 'default',
      useFactory: async (configService: ConfigService) => {
        const options: MongooseModuleOptions = {
          uri: configService.db.uri,
          // dbName: configService.db.database,
          //user: configService.db.username,
          // pass: configService.db.password,
          useNewUrlParser: true,
          useUnifiedTopology: true,
          autoCreate: true,
        };

        return options;
      },
      inject: [ConfigService],
    }),
  ],
  providers: [],
  exports: [],
})
export class DatabaseModule {}
