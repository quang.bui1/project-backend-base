import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Transform } from 'class-transformer';
import {
  UserStatus,
  UserGender,
  UserRole,
} from '../../modules/auth/auth.constant';

import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document, ObjectId } from 'mongoose';

export type UserDocument = User & Document;

@Schema()
export class User {
  @Transform(({ value }) => value.toString())
  _id: ObjectId;

  @Prop({ type: String, length: 255 })
  @ApiProperty({ description: 'Email user' })
  email: string;

  @Prop({ length: 255, nullable: true })
  @Exclude()
  password: string;

  @Prop({ length: 100, default: '', nullable: true })
  @ApiProperty()
  firstName: string;

  @Prop({ length: 100, default: '', nullable: true })
  @ApiProperty()
  lastName: string;

  @Prop({ nullable: true })
  @ApiProperty()
  birthday: string;

  @Prop({ length: 50, nullable: true })
  @ApiProperty()
  phoneNumber: string;

  @Prop({ length: 2000, nullable: true })
  @ApiProperty()
  address: string;

  @Prop({
    type: String,
    enum: UserGender,
    nullable: true,
  })
  @ApiProperty()
  gender: UserGender;

  @Prop({
    type: String,
    enum: UserRole,
    default: UserRole.USER,
  })
  @ApiProperty()
  role: UserRole;

  @Prop({
    type: String,
    enum: UserStatus,
    default: UserStatus.PENDING,
  })
  @ApiProperty()
  status: UserStatus;

  @Prop({ type: Date, nullable: true })
  @ApiProperty({ type: 'number', example: 1546300800 })
  lastLoginAt: Date;

  @Prop({ type: Date })
  @ApiProperty({ type: 'number', example: 1546300800 })
  createdAt: Date;

  @Prop({ type: Date })
  @ApiProperty({ type: 'number', example: 1546300800 })
  updatedAt: Date;

  @Prop({ type: Date })
  deletedAt: Date;

  @Prop({ type: 'number' })
  @ApiProperty({ type: 'number' })
  createdBy: number;

  @Prop({ type: 'number' })
  @ApiProperty({ type: 'number' })
  updatedBy: number;

  @Prop({ type: 'number' })
  @ApiProperty({ type: 'number' })
  deletedBy: number;
}

const userSchema = SchemaFactory.createForClass(User);

export const UserSchema = userSchema;
