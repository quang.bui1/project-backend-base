import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Transform } from 'class-transformer';
import { UserGender } from '../../modules/auth/auth.constant';
import { Class, ClassSchema } from './class.entity';

import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document, ObjectId, Schema as MongoSchema } from 'mongoose';

export type StudentDocument = Student & Document;

@Schema()
export class Student {
  @Transform(({ value }) => value.toString())
  _id: ObjectId;

  @Prop({ type: String, length: 40 })
  @ApiProperty({ description: 'student name' })
  fullName: string;

  @ApiProperty()
  gender: UserGender;

  @Prop({ type: 'number' })
  @ApiProperty({ type: 'number' })
  age: number;

  @Prop({
    type: MongoSchema.Types.ObjectId,
    ref: 'ClassSchema',
    nullable: true,
  })
  @ApiProperty({ description: 'class Name', required: false })
  class: Class;

  @Prop({ type: 'number' })
  @ApiProperty({ type: 'number' })
  mathScore: number;

  @Prop({ type: 'number' })
  @ApiProperty({ type: 'number' })
  englishScore: number;

  @Prop({ type: Date })
  @ApiProperty({ type: 'number', example: 1546300800 })
  createdAt: Date;

  @Prop({ type: Date })
  @ApiProperty({ type: 'number', example: 1546300800 })
  updatedAt: Date;

  @Prop({ type: Boolean, default: false })
  deleted: Boolean;
}

const studentSchema = SchemaFactory.createForClass(Student);

export const StudentSchema = studentSchema;
