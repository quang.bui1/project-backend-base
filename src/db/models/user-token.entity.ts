import { ApiProperty } from '@nestjs/swagger';
import { User, UserSchema } from './user.entity';
import { UserTokenType } from '../../modules/auth/auth.constant';
import { ObjectId, Schema as MongoSchema, Document } from 'mongoose';
import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Transform } from 'class-transformer';

@Schema({ id: true })
export class UserToken {
  @Transform(({ value }) => value.toString())
  _id: ObjectId;

  @Prop({ type: MongoSchema.Types.ObjectId, ref: 'UserSchema' })
  user: User;

  // hash token value to find faster
  @Prop({ type: String })
  hashToken: string;

  @Prop({ type: MongoSchema.Types.Buffer })
  @ApiProperty()
  token: string;

  @Prop({
    type: String,
    enum: UserTokenType,
    default: UserTokenType.REFRESH_TOKEN,
  })
  @Prop({ type: Date })
  @ApiProperty({ type: Number, example: 1546300800 })
  createdAt: Date;

  @Prop({ type: Date, nullable: true })
  @ApiProperty({ type: Number, example: 1546300800 })
  deletedAt: Date;
}

export const UserTokenSchema = SchemaFactory.createForClass(UserToken);
export type UserTokenDocument = UserToken & Document;
