import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Transform } from 'class-transformer';
import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document, ObjectId } from 'mongoose';

export type ClassDocument = Class & Document;

@Schema()
export class Class {
  @Transform(({ value }) => value.toString())
  _id: ObjectId;

  @Prop({ type: String, length: 40 })
  @ApiProperty({ description: 'class name' })
  className: string;

  @Prop({ type: String, length: 40 })
  @ApiProperty({ description: 'school name' })
  schoolName: string;

  @Prop({ type: String, length: 50 })
  @ApiProperty({ description: 'teacher name' })
  teacherName: string;

  @Prop({ type: Date })
  @ApiProperty({ type: 'number', example: 1546300800 })
  createdAt: Date;

  @Prop({ type: Date })
  @ApiProperty({ type: 'number', example: 1546300800 })
  updatedAt: Date;

  @Prop({ type: Boolean, default: false })
  deleted: Boolean;
}

const classSchema = SchemaFactory.createForClass(Class);

export const ClassSchema = classSchema;
