import { Module } from '@nestjs/common';
import { DatabaseModule } from './db/database.module';
import { AuthModule } from './modules/auth/auth.module';
import { CommonModule } from './modules/common/common.module';
import { WinstonModule } from './modules/common/services/winston.service';
import { UsersModule } from './modules/users/users.module';
import { StudentsModule } from './modules/students/students.module';
import { ClassModule } from './modules/classes/classes.module';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    WinstonModule,
    CommonModule,
    DatabaseModule,
    AuthModule,
    UsersModule,
    StudentsModule,
    ClassModule,
    ScheduleModule.forRoot(),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
