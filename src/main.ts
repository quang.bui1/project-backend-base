import { ValidationPipe } from '@nestjs/common';
import { CorsOptions } from '@nestjs/common/interfaces/external/cors-options.interface';
import { NestFactory } from '@nestjs/core';
import { NestExpressApplication } from '@nestjs/platform-express';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import * as compression from 'compression';
import { AppModule } from './app.module';
import { ConfigService } from './modules/common/services/config.service';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  app.useGlobalPipes(new ValidationPipe());
  const configService: ConfigService = app.get(ConfigService);
  const options = new DocumentBuilder()
    .setTitle('API Docs - Core')
    .setVersion('1.0')
    .addBearerAuth()
    .addServer(
      `${[configService.domain, configService.basePath].join('')}`,
      '',
      {},
    )
    .build();

  const document = SwaggerModule.createDocument(app, options);

  SwaggerModule.setup('api', app, document);

  const whitelist = configService.cors;
  const corsOptions: CorsOptions = {
    origin: whitelist,
    allowedHeaders: ['Content-Type', 'Authorization', 'Language'],
    optionsSuccessStatus: 200,
    methods: ['GET', 'PUT', 'POST', 'PATCH', 'DELETE', 'HEAD', 'OPTIONS'],
  };

  app.enableCors(corsOptions);
  app.setGlobalPrefix(configService.basePath);

  // Compression can greatly decrease the size of the response body, thereby increasing the speed of a web app.
  app.use(compression());
  await app.listen(configService.port);
}
bootstrap();
